"""
Anthony Smith
SDEV300
LAB 4
Matrix
"""
import numpy as np

#method to creat matrix
def createMatrix():
    matrix = np.array([[int(input()),int(input()),int(input())],
                    [int(input()),int(input()),int(input())],
                    [int(input()),int(input()),int(input())]])
    return matrix

#tranpose method
def matrixTranspose(matrix):
    return matrix.T

#addition method
def addition(matrix1,matrix2):
    rows = 3
    columns = 3
    for i in range(columns):
        for j in range(rows):
            matrix3 = matrix1 + matrix2
            return matrix3
            
#multiplication method            
def multiplication(matrix1, matrix2):
        rows = 3
        columns = 3
        for i in range(columns):
            for j in range(rows):
                matrix3 = matrix1 * matrix2
                return matrix3

#subtraction method                
def subtraction(matrix1, matrix2):
    rows = 3
    columns = 3
    for i in range(columns):
        for j in range(rows):
            matrix3 = matrix1 - matrix2
            return matrix3
#matrix multiplication method            
def matrixMultiplication(matrix1,matrix2):
    matrix3 = np.dot(matrix1,matrix2)
    return matrix3

#calc the mean of a row
def rowMean(matrix):
    matrix3 = matrix.mean(axis=0)
    return matrix3

#calc the mean of a column    
def columnMean(matrix):
    matrix3 = matrix.mean(axis=1)
    return matrix3

#validate the user is inputing integers and create matrix
while(True):
    try:
        print("Enter a 3x3 matrix")
        matrix1 = createMatrix()
        print("Enter a second 3x3 matrix")
        matrix2 = createMatrix()
        break
    except ValueError:
        print("enter numeric values only")



#default value for choice
choice = ''
answer = ''
#while look to prompt user and keep program running until user enters N
while(answer != 'N'):
    answer = input("Do you want to play the Matrix Game? Enter Y for Yes or N for No: ")
    if answer == 'N':
        exit(0)
    else:
        print("""
        a. Addition
        b. Subtraction
        c. Matrix Multiplication
        d. Element by element multiplication""")
        #take users choice
        choice = input()
        #output matix
        print("\n Matrix 1:")
        print(*matrix1[0])
        print(*matrix1[1])
        print(*matrix1[2])
        print("\n Matrix 2:")
        print(*matrix2[0])
        print(*matrix2[1])
        print(*matrix2[2])
    
    
    """if statment to run method that the user chose and to output 
    the transponse and mean"""
    if(choice == 'a'):
        print("\nAddition:")
        print("")
        addedMatrix = addition(matrix1,matrix2)
        for i in range(len(addedMatrix)):
            print(*addedMatrix[i])
        print("")
        print("Transponse: ")
        transponse1 = matrixTranspose(addedMatrix)
        for i in range(len(transponse1)):
            print(*transponse1[i])
        print("")
        print("Row Mean")
        print(rowMean(transponse1))
        print("Column mean:")
        print(columnMean(transponse1))
    elif(choice == 'b'):
        print("\nSubtraction:")
        print("")
        subtractedMatrix = subtraction(matrix1,matrix2)
        for i in range(len(subtractedMatrix)):
            print(*subtractedMatrix[i])
        print("")
        print("Transponse: ")
        transponse1 = matrixTranspose(subtractedMatrix)
        for i in range(len(transponse1)):
            print(*transponse1[i])
        print("")
        print("Row Mean")
        print(rowMean(transponse1))
        print("Column mean:")
        print(columnMean(transponse1))
    elif(choice == 'c'):
        print("\nMatrix Multiplication:")
        print("")
        matrixMultMatrix = matrixMultiplication(matrix1, matrix2)
        for i in range(len(matrixMultMatrix)):
            print(*matrixMultMatrix[i])
        print("")
        print("Transponse: ")
        transponse1 = matrixTranspose(matrixMultMatrix)
        for i in range(len(transponse1)):
            print(*transponse1[i])
        print("")
        print("Row Mean")
        print(rowMean(transponse1))
        print("Column mean:")
        print(columnMean(transponse1))
    elif(choice == 'd'):
        print("\n Multiplication:")
        print("")
        MultMatrix = multiplication(matrix1, matrix2)
        for i in range(len(MultMatrix)):
            print(*MultMatrix[i])
        print("")
        print("Transponse: ")
        transponse1 = matrixTranspose(MultMatrix)
        for i in range(len(transponse1)):
            print(*transponse1[i])
        print("")
        print("Row Mean")
        print(rowMean(transponse1))
        print("Column mean:")
        print(columnMean(transponse1))

    
