"""
Anthony Smith
SDEV300
LAB 4
Customer Info
"""
#import pandas and re
import pandas as pd
import re

#method to format phone number
def formatted_number(phone):
    result = re.fullmatch(r'(\d{3})(\d{3})(\d{4})', phone)
    if len(phone) > 13:
        phone = ''
    return '-'.join(result.groups()) if result else phone

#method to format zip
def formatted_zip(zip):
    if len(zip) < 5 or len(zip) > 9 :
        zip = ' '
        return zip
    elif len(zip) ==9:
        zip = zip[:5] + "-" + zip[5:]
        return zip
    else:    
        return zip

#method to format email
def formatted_email(email):
    for i in email:
        if '@' not in email or '.' not in email:
            email = ''
            return email
        else:
            return email
            
#array to hold all customer info
customer_info = [['Jim Robertson', '000000000', '555-555-5555', 'jim@gmail.com'],
                 ['John Adams', '11143', '4444444444', 'john@gmail.com'],
                 ['Thomas Jefferson', '456907259', '4444444444', 'tommy@gmailcom'],
                 ['John Adams', '111', '8569631265', 'johngmail.com']]

#assign column data frame
customer_infoFormat = pd.DataFrame(customer_info, columns=['Name', 'zip', 'Phone','Email'])

#use method to correct in correct data or to remove data
corrected_phone = customer_infoFormat['Phone'].map(formatted_number)
corrected_zip = customer_infoFormat['zip'].map(formatted_zip)
corrected_email = customer_infoFormat['Email'].map(formatted_email)

#replace original data with corrected data
customer_infoFormat['Phone'] = corrected_phone
customer_infoFormat['zip'] = corrected_zip
customer_infoFormat['Email'] = corrected_email

#output 
print(customer_infoFormat)
